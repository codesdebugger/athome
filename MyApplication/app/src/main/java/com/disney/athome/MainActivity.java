package com.disney.athome;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;
import android.webkit.WebSettings;

import com.wdpr.ee.ra.webviewbridge.WebViewBridge;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private String webSiteURL = "https://disneycruise.disney.go.com/";
    private WebViewBridge webViewBridge;
    //    private String webSiteURL = "www.google.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = (WebView) findViewById(R.id.webview);
        webViewBridge = new WebViewBridge.Builder()
                .setWebView(webView)
                .setUrl(webSiteURL)
                .setWebViewClient(new DisneyWebClient())
                .setWebChromeClient(new DisneyChromeClient() ).build();
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.getSettings().setAppCacheMaxSize(1024*1024*8);
//        webView.getSettings().setAppCachePath(getApplicationInfo().dataDir+"/cache"‌​);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowContentAccess(true);

        webView.addJavascriptInterface(new WebAppInterface(this) {

            @JavascriptInterface
            public void showLogindDialog() {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Title").setMessage("Message").setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(mContext, "Signin click", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        webView.post(new Runnable() {
                            @Override
                            public void run() {
                                webView.goBack();
                                webView.goBack();
                            }
                        });
                    }
                });
                builder.show();

            }
        }, "android");
        //set URL address:
        webView.loadUrl(webSiteURL);

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

}
