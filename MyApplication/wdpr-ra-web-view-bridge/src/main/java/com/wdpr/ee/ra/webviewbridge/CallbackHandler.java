package com.wdpr.ee.ra.webviewbridge;

/**
 * Created by kalak012 karthik on 1/26/17.
 *
 * Callback interface , can be passed as a closure on method params
 */

public interface CallbackHandler<T> {

    public void onMessage(T commandName,T data);
};

