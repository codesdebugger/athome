package com.wdpr.ee.ra.webviewbridge;

import java.util.Arrays;

/**
 * Created by hoodp003 on 1/12/17.
 *
 * Model class to pass javascript method name and params
 */

public class JavaScriptMethod {
    public final String methodName;
    public final String[] parameters;


    public JavaScriptMethod(String methodName, String[] parameters) {
        if(methodName == null || methodName.trim().length() == 0) throw new IllegalStateException("Missing methodName");
        this.methodName = methodName;
        this.parameters = parameters;
    }

    /**
     * Construct javascript function with params. THe generated function will be executed on webview.
     * Currently only params containing quotes and backslash are escaped
     *
     * @return
     */
    public final String getJavaScriptString() {
        String jsScript = "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(methodName);
        stringBuilder.append("(");
        if(parameters!=null && parameters.length !=0)
        {
            for(String param: Arrays.asList(parameters))
            {
                if(param==null)
                {
                    stringBuilder.append(param);
                    stringBuilder.append(",");
                }else
                {
                    stringBuilder.append("'");
                    stringBuilder.append(escapeSpecialChar(param));
                    stringBuilder.append("'");
                    stringBuilder.append(",");
                }

            }
        }

        jsScript = stringBuilder.toString();
        if(parameters!=null && parameters.length !=0)
            jsScript = jsScript.substring(0, jsScript.lastIndexOf(","));
        jsScript = jsScript + (");");
        return jsScript;
    }

    /**
     * Escapes special char add more special char checks as needed.
     *
     * @param param
     * @return returns  param with special chars striped.
     */
    private String escapeSpecialChar(String param)
    {
        if(param!=null )
        {
            if(param.contains("\\"))
                param = param.replace("\\", "\\\\");
            if(param.contains("'"))
                param = param.replace("\'", "\\\'");

        }

        return param;
    }
}
