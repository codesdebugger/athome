package com.wdpr.ee.ra.webviewbridge;

import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import java.util.HashMap;

/**
 * Created by hoodp003 on 1/12/17.
 * Updated by kalak012 karthik 1/30/17
 *
 * This class is the main class that handles Native to WebView and WebView to Native communication.
 *
 */

public class WebViewBridge {
    private static final String TAG = "WebViewBridge";
    private static final String JAVASCRIPT_INTERFACE_NAME = "disneyRAWebViewBridge";
    private final WebView webView;
    //Webview events
    private static final String EVENT_READY = "disneyRAWebViewBridgeReady";
    private static final String EVENT_CLOSE = "disneyRAWebViewBridgeClose";
    private static final String EVENT_ERROR = "disneyRAWebViewBridgeError";
    private final String url;
    private final WebViewClient webViewClient;
    private final WebChromeClient webChromeClient;
    //Map that holds the events/actions to CallbackHandler mapping
    private final HashMap<String,CallbackHandler<String>> callbackHandlerMapping;

    /**
     * WebViewBridge constructor sets the required objects and configures webview
     *
     * @param webView
     * @param url
     * @param webViewClient
     * @param webChromeClient
     * @param callbackHandlerMapping
     */
    private WebViewBridge(WebView webView, String url, WebViewClient webViewClient,WebChromeClient webChromeClient,
                          HashMap<String,CallbackHandler<String>> callbackHandlerMapping) {
        this.webView = webView;
        this.url = url;
        this.webViewClient = webViewClient;
        this.webChromeClient = webChromeClient;
        this.callbackHandlerMapping = callbackHandlerMapping;
        webView.setWebViewClient( webViewClient );
        webView.setWebChromeClient( webChromeClient );

        //Additional settings
        webView.addJavascriptInterface(this, JAVASCRIPT_INTERFACE_NAME);
        webView.getSettings().setJavaScriptEnabled(true);
    }

    public WebView getWebView()
    {
        return webView;
    }
    public WebViewClient getWebViewClient()
    {
        return webViewClient;
    }
    public WebChromeClient getWebChromeClient()
    {
        return webChromeClient;
    }

    /**
     * Builder to create WebViewBridge instance
     */
    public static class Builder {
        private WebView webView;
        private String url;
        private WebViewClient webViewClient;
        private HashMap<String,CallbackHandler<String>> callbackHandlerMapping = new HashMap<String,CallbackHandler<String>>();
        private WebChromeClient webChromeClient;

        public Builder setWebView(WebView webView)
        {
            this.webView = webView;
            return this;
        }

        public Builder setUrl(String url)
        {
            Log.d(TAG,url);
            this.url = url;
            return this;
        }

        public Builder setWebViewClient(WebViewClient webViewClient)
        {
            this.webViewClient = webViewClient;
            return this;
        }

        public Builder setWebChromeClient(WebChromeClient webChromeClient)
        {
            this.webChromeClient = webChromeClient;
            return this;
        }

        public Builder addWebViewReadyHandler(CallbackHandler<String> webViewReadyEvent)
        {
            callbackHandlerMapping.put(EVENT_READY.toLowerCase() , webViewReadyEvent);
            return this;
        }

        public Builder addWebViewErrorHandler(CallbackHandler<String> webViewErrorEvent)
        {
            callbackHandlerMapping.put(EVENT_ERROR.toLowerCase() , webViewErrorEvent);
            return this;
        }

        public Builder addWebViewCloseHandler(CallbackHandler<String> webViewCloseEvent)
        {
            callbackHandlerMapping.put(EVENT_CLOSE.toLowerCase() , webViewCloseEvent);
            return this;
        }

        public Builder addWebViewCustomHandler(String actionType,CallbackHandler<String>  webViewCustomHandler)
        {
            callbackHandlerMapping.put(actionType.toLowerCase() , webViewCustomHandler);
            return this;
        }

        public WebViewBridge build()
        {
            if(webView == null) throw new IllegalStateException("WebView Object cannot be null");

            if(webViewClient != null)
            webView.setWebViewClient(webViewClient);

            if(webChromeClient != null)
            webView.setWebChromeClient(webChromeClient);

            return new WebViewBridge(webView,url,webViewClient,webChromeClient,callbackHandlerMapping);
        }
    }

    /**
     * This method accepts javascript method and params that will be executed on the webview
     *
     * @param methodName , JavaScrip method name
     * @param parameters , optional params that will be passed to the javascript method.
     */
    public void evaluateJavascript(String methodName, String[] parameters) {

        if(methodName == null) throw new IllegalStateException("Missing methodName argument");
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod(methodName,parameters);
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Log.d(TAG,"evaluateJavascript:" + constructedJsMethod);
        webView.evaluateJavascript(constructedJsMethod, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                Log.d(TAG, s);
                //I don't care about the callback
            }
        });
    }


    /**
     * This method is exposed to webview as javascript method disneyRAWebViewBridge.sendMessage( commandName,params,completionCallback)
     *
     * @param commandName : Can be any name that will be mapped to configured handler callback's
     * @param params      : Can be any string (  string , json etc )
     * @param completionCallback    : completion Callback that will be invoked to notify webview on command completion
     */
    @JavascriptInterface
    public void postMessage(String commandName, String params, final String completionCallback) {
        Log.d(TAG,"\n\nReceived commandName[" + commandName +"]");
        Log.d(TAG,"Params:" + params);
        Log.d(TAG,"Received command completion callback [" + completionCallback +"]");

        if(callbackHandlerMapping.get(commandName.toLowerCase()) != null ) {
            //Execute the command handler provided by native passing the command name and params
            callbackHandlerMapping.get(commandName.toLowerCase()).onMessage(commandName,params);
        }else {
            Log.d(TAG,"No command mapper found to handle [" + commandName+"]");
        }

        //Once the command handling is completed notify webview by invoking completion callback
        //so that webview can process the next message
        final String constructedJsMethod = "window."+completionCallback;
        Log.d(TAG,"evaluateJavascript:" + constructedJsMethod);
        //Since this method ( annotated with @JavascriptInterface ) runs on 'JavaBridge' thread.
        //We need fork a thread to evaluate javascript on a separate thread.
        webView.post(new Runnable() {
            @Override
            public void run() {
                evaluateJavascript(constructedJsMethod,null);
            }
        });
    }

}
