package com.wdpr.ee.ra.webviewbridge;

import android.content.Context;
import android.test.mock.MockContext;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kalak012 on 2/1/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class WebViewBridgeTest {

    @Mock
    WebView webView;

    @Mock
    WebSettings webSettings;

    String url = "https://jsbin.com/boyafeleci?platform=android";

    @Before
    public void setup()
    {
        webView.setWebViewClient(new MockWebClient());
        Mockito.when(webView.getSettings()).thenReturn( webSettings );

    }

    @Test
    public void testCreatingWebViewBridge()
    {
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                                        .setWebView(webView)
                                        .setWebViewClient(webViewClient)
                                        .setWebChromeClient( new WebChromeClient())
                                        .setUrl("https://google.com")
                                        .build();
        webView.loadUrl(url);

        Assert.assertNotNull(webViewBridge.getWebView());
        Assert.assertNotNull(webViewBridge.getWebViewClient());
        Assert.assertNotNull(webViewBridge.getWebChromeClient());

    }

    @Test(expected=IllegalStateException.class)
    public void testCreatingWebViewBridgeWithNullWebView()
    {
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(null)
                .setWebViewClient(webViewClient)
                .build();
        webView.loadUrl(url);

    }

    @Test
    public void testAddingEventHandlers()
    {
        final List<String> mockList = new ArrayList<>();//to keep track of callbacks received
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(webView)
                .setWebViewClient(webViewClient)
                .addWebViewReadyHandler(new CallbackHandler<String>() {
                    @Override
                    public void onMessage(String commandName, String data) {
                        Assert.assertEquals(commandName,"disneyRAWebViewBridgeReady");
                        mockList.add("disneyRAWebViewBridgeReady");
                    }
                })
                .addWebViewCloseHandler(new CallbackHandler<String>() {
                    @Override
                    public void onMessage(String commandName, String data) {
                        Assert.assertEquals(commandName,"disneyRAWebViewBridgeClose");
                        mockList.add("disneyRAWebViewBridgeClose");
                    }
                }).addWebViewErrorHandler(new CallbackHandler<String>() {
                    @Override
                    public void onMessage(String commandName, String data) {
                        Assert.assertEquals(commandName,"disneyRAWebViewBridgeError");
                        mockList.add("disneyRAWebViewBridgeError");
                    }
                })
                .build();

        webView.loadUrl(url);

        //Simulate calling JavascriptInterface method
        webViewBridge.postMessage("disneyRAWebViewBridgeReady",null,"none");
        webViewBridge.postMessage("disneyRAWebViewBridgeClose",null,"none");
        webViewBridge.postMessage("disneyRAWebViewBridgeError",null,"none");

        Assert.assertTrue("No handler was invoked for disneyRAWebViewBridgeReady",mockList.contains("disneyRAWebViewBridgeReady"));
        Assert.assertTrue("No handler was invoked for disneyRAWebViewBridgeClose",mockList.contains("disneyRAWebViewBridgeClose"));
        Assert.assertTrue("No handler was invoked for disneyRAWebViewBridgeError",mockList.contains("disneyRAWebViewBridgeError"));
    }

    @Test
    public void testAddingCustomHandlers()
    {
        final List<String> mockList = new ArrayList<>();//to keep track of callbacks received
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(webView)
                .setWebViewClient(webViewClient)
                .addWebViewCustomHandler("removeToken", new CallbackHandler<String>() {
                    @Override
                    public void onMessage(String commandName, String data) {
                        Assert.assertEquals(commandName,"removeToken");
                        mockList.add("removeToken");
                    }
                })
                .build();

        webView.loadUrl(url);

        //Simulate calling JavascriptInterface method
        webViewBridge.postMessage("removeToken",null,"none");

        Assert.assertTrue(mockList.contains("removeToken"));
    }

    @Test
    public void testMissingCommandMapper()
    {
        CustomMockWebView customMockWebView = new CustomMockWebView(new MockContext());
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(customMockWebView)
                .setWebViewClient(webViewClient)
                .build();

        webView.loadUrl(url);
        webViewBridge.postMessage("command" , null,"callback");
        Assert.assertFalse(customMockWebView.isPostMessageCallBackCalled());
    }

    @Test(expected=IllegalStateException.class)
    public void testEvalJSMethodWMissingMethodName()
    {
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(webView)
                .setWebViewClient(new WebViewClient())
                .build();

        webView.loadUrl(url);

        webViewBridge.evaluateJavascript(null,null);
    }


    @Test
    public void testEvalJSMethodWithCustomMockWebview()
    {
        CustomMockWebView customMockWebView = new CustomMockWebView(new MockContext());
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(customMockWebView)
                .setWebViewClient(webViewClient)
                .addWebViewCustomHandler("command", new CallbackHandler<String>() {
                    @Override
                    public void onMessage(String commandName, String data) {
                        Assert.assertEquals(commandName,"command");
                    }
                })
                .build();

        webView.loadUrl(url);
        String jsMethod = "testJSMethod";
        webViewBridge.evaluateJavascript(jsMethod,null);
        webViewBridge.postMessage("command" , null,"callback");
        Assert.assertTrue(customMockWebView.isEvalJSCalled());
    }



    @Test
    public void testEvalJSMethodIsCalledWithMissingCommandHandler()
    {
        CustomMockWebView customMockWebView = new CustomMockWebView(new MockContext());
        WebViewClient webViewClient = new MockWebClient();
        WebViewBridge webViewBridge = new WebViewBridge.Builder()
                .setWebView(customMockWebView)
                .setWebViewClient(webViewClient)
                .build();

        webView.loadUrl(url);
        String jsMethod = "testJSMethod";
        webViewBridge.postMessage("command" , null,"callback");
        Assert.assertTrue(customMockWebView.isEvalJSCalled());
    }


    public class MockWebClient extends WebViewClient {

    }

    public class CustomMockWebView extends WebView
    {
        boolean isEvalJSCalledFlag = false;
        boolean isPostMessageCallBackCalledFlag = false; //This flag is to check if the command handler was executed and callback was called
        public CustomMockWebView(Context context) {
            super(context);
        }

        @Override
        public void evaluateJavascript(String script, ValueCallback<String> resultCallback) {
            isEvalJSCalledFlag = true;
            resultCallback.onReceiveValue("testReturnValue");
        }

        @Override
        public WebSettings getSettings() {
            return webSettings;
        }

        @Override
        public boolean post(Runnable action) {
            isPostMessageCallBackCalledFlag = true;
            action.run();
            return true;
        }

        public boolean isEvalJSCalled()
        {
            return isEvalJSCalledFlag;
        }

        public boolean isPostMessageCallBackCalled()
        {
            return isPostMessageCallBackCalledFlag;
        }
    }
}
