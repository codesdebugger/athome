package com.wdpr.ee.ra.webviewbridge;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by kalak012 on 2/2/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class JavaScriptMethodTest {

    @Test
    public void testJavaScriptMethodWithNullParams()
    {
        String expectedJsMethod = "showLog();";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",null);
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithEmptyParams()
    {
        String expectedJsMethod = "showLog();";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithParams()
    {
        String expectedJsMethod = "showLog('message');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithMultiParams()
    {
        String expectedJsMethod = "showLog('message1','message2');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1","message2"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));

    }

    @Test
    public void testJavaScriptMethodWithSingleQuote()
    {
        //Test params containing quotes
        String expectedJsMethod = "showLog('Message\\\'WithQuote');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"Message'WithQuote"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithMultiParamsAndQuote()
    {
        String expectedJsMethod = "showLog('message1','message2','Message\\\'WithQuote');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1","message2","Message'WithQuote"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithBackSlash()
    {
        //Test params containing back slash
        String expectedJsMethod = "showLog('message1','message2','Message\\\\WithSlash');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1","message2","Message\\WithSlash"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithQuoteAndSlash()
    {
        //Test params containing quote and back slash
        String expectedJsMethod = "showLog('message1','message2','Message\\\'WithQuote','Message\\\\WithSlash');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1","message2","Message'WithQuote","Message\\WithSlash"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithQuoteAndSlashInSingleParam()
    {
        //Test params containing quote and back slash
        String expectedJsMethod = "showLog('message1','message2','Message\\\'WithQuote And \\\\WithSlash');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1","message2","Message'WithQuote And \\WithSlash"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test(expected = IllegalStateException.class)
    public void testJavaScriptMethodMissingMethodName()
    {
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod(null,null);
        javaScriptMethod.getJavaScriptString();
    }

    @Test
    public void testJavaScriptMethodWithCombinationOfNullParam()
    {
        String expectedJsMethod = "showLog('message1',null,'message2');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1",null,"message2"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }

    @Test
    public void testJavaScriptMethodWithCombinationOfEmptyAndNullParams()
    {
        String expectedJsMethod = "showLog('message1',null,'message2','','message3');";
        JavaScriptMethod javaScriptMethod = new JavaScriptMethod("showLog",new String[]{"message1",null,"message2","","message3"});
        String constructedJsMethod = javaScriptMethod.getJavaScriptString();
        Assert.assertTrue("JavaScript method constructed ["+constructedJsMethod+"] expected ["+expectedJsMethod+"]",constructedJsMethod.equals(expectedJsMethod));
    }
}
